<?php

namespace Pratt\Tests;

use Silex\WebTestCase;

class RecipeGetCuisineTest extends WebTestCase
{
    public function createApplication()
    {
        return require __DIR__ . '/../index.php';
    }

    public function testSuccessful()
    {
        $client = $this->createClient();
        $client->request('GET', '/recipes?cuisine=italian&page=1');
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $content = $response->getContent();
        $this->assertJson($content);

        $recipes = json_decode($content, true);
        foreach ($recipes as $recipe) {
            $this->assertEquals('italian', $recipe['recipe_cuisine']);
        }
    }

    public function testPagination()
    {
        $client = $this->createClient();
        $client->request('GET', '/recipes?cuisine=asian&page=1');
        $firstPageContent = json_decode($client->getResponse()->getContent(), true);
        $client->request('GET', '/recipes?cuisine=asian&page=2');
        $secondPageContent = json_decode($client->getResponse()->getContent(), true);

        $this->assertNotEquals($firstPageContent, $secondPageContent);
    }

    public function testInvalidCuisine()
    {
        $client = $this->createClient();
        $client->request('GET', '/recipes?cuisine=car');

        $this->assertTrue($client->getResponse()->isNotFound());
    }
}