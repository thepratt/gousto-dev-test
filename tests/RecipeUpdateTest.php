<?php

namespace Pratt\Tests;

use Silex\WebTestCase;

class RecipeUpdateTest extends WebTestCase
{
    public function createApplication()
    {
        return require __DIR__ . '/../index.php';
    }

    public function testSuccessful()
    {
        $client = $this->createClient();
        $client->request(
            'PUT',
            '/recipes/1',
            [],
            [],
            [],
            json_encode([
                'title' => sprintf('title%u', rand(1, 100)),
                'slug' => sprintf('slug%u', rand(1, 100)),
                'gousto_reference' => rand(1, 100)
            ])
        );

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testRequirements()
    {
        $client = $this->createClient();
        $client->request('PUT', '/recipes/1');

        $this->assertTrue($client->getResponse()->isClientError());
    }
}