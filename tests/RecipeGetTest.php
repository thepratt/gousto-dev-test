<?php

namespace Pratt\Tests;

use Silex\WebTestCase;

class RecipeGetTest extends WebTestCase
{
    public function createApplication()
    {
        return require __DIR__ . '/../index.php';
    }

    public function testSuccessful()
    {
        $client = $this->createClient();
        $client->request('GET', '/recipes/1');
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());

        $content = $response->getContent();
        $this->assertJson($content);

        $obj = json_decode($content, true);
        $this->assertEquals($obj['id'], 1);
    }

    public function testNotFound()
    {
        $client = $this->createClient();
        $client->request('GET', '/recipes/99');

        $this->assertTrue($client->getResponse()->isNotFound());
    }
}