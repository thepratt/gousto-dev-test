<?php

namespace Pratt\Tests;

use Silex\WebTestCase;

class RecipeRatingAddTest extends WebTestCase
{
    public function createApplication()
    {
        return require __DIR__ . '/../index.php';
    }

    public function testSuccessful()
    {
        $client = $this->createClient();
        $client->request(
            'POST',
            '/recipes/1/rating',
            [],
            [],
            [],
            json_encode([
                'rating' => 4
            ])
        );
        $response = $client->getResponse();

        $this->assertTrue($response->isOk());
    }

    public function testNotFound()
    {
        $client = $this->createClient();
        $client->request(
            'POST',
            '/recipes/99/rating',
            [],
            [],
            [],
            json_encode([
                'rating' => 3
            ])
        );

        $this->assertTrue($client->getResponse()->isNotFound());
    }

    public function testRequirements()
    {
        $client = $this->createClient();
        $client->request('POST', '/recipes/1/rating');

        $this->assertTrue($client->getResponse()->isClientError());
    }
}