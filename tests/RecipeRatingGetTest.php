<?php

namespace Pratt\Tests;

use Silex\WebTestCase;

class RecipeRatingGetTest extends WebTestCase
{
    public function createApplication()
    {
        return require __DIR__ . '/../index.php';
    }

    public function testSuccessful()
    {
        $client = $this->createClient();
        $client->request('GET', '/recipes/1/rating');
        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());

        $content = $response->getContent();
        $this->assertJson($content);

        $obj = json_decode($content, true);
        $this->assertGreaterThan(1, $obj['rating']);
        $this->assertLessThan(5, $obj['rating']);
    }

    public function testNotFound()
    {
        $client = $this->createClient();
        $client->request('GET', '/recipes/99/rating');

        $this->assertTrue($client->getResponse()->isNotFound());
    }
}