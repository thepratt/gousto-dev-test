# Gusto Developer Test - Sam Pratt

## Installation/Setup
### Requirements:
- PHP 5.6 (version tested against)
- composer

### To Install:
1. `composer install`

### Starting server:
Using the built-in php web-server (PHP 5.4+ only) we can run the application for field testing. In the root of the directory,
where `index.php` is located, running `php -S localhost:1234` will open the application for usage locally on port `1234`.


## Tests
Functional tests written with PHPUnit, using the Symfony WebCase are provided. These can be run with the command `phpunit`.

## Developer Questions
### Why did I choose Silex?
There are 3 main points as to why I chose Silex:
- I wanted to keep in the PHP ecosystem, otherwise I would've chosen Finatra (Scala) for ease, and speed of development;
- I am familiar with the Symfony component ecosystem, and have used Silex before;
- Complexity of a large scale application that one would use Ruby on Rails, Symfony2, or other was not needed.

### How would I change the application to cater for mobile/front-end?
Apart from setting up caching properly (optimization for server, not strictly the phone), I wouldn't need to change the 
application to cater for mobile as Android, iOS, and Windows can all consume a JSON API just fine. An addition I would 
make would be HATEOAS links to allow for proper navigation between resources.  
If the front-end were to be a separate application (e.g. Vue.js, React, Angular), the view-layer can be treated the same 
as mobile above. If the views were to be part of an MVC structure/bundled with the application, I would append .json to 
the end of API methods. This would allow the view, and the API to function harmoniously. The other option is to split the 
API, and views to `/api/recipes/*`, and `/recipes/*` which is another also acceptable solution.

### Notes:
- I have only implemented `PUT` as a full update (upsert), and not touched `PATCH` for partial updating of fields.
The only reason is there are a lot of fields for the test entity.
- Some of the tests that depend on ids only conditionally pass. The condition is they won't be spammed with data (or enough 
to create more than 99 entries).