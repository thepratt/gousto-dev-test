<?php

require_once __DIR__ . '/vendor/autoload.php';

use Pratt\ServiceProvider\ControllerServiceProvider;
use Pratt\ServiceProvider\NormalizerServiceProvider;
use Pratt\ServiceProvider\RepositoryServiceProvider;
use Silex\Application;
use Silex\Provider\ServiceControllerServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Symfony\Component\Validator\Mapping\Factory\LazyLoadingMetadataFactory;
use Symfony\Component\Validator\Mapping\Loader\YamlFileLoader;

$app = new Application();

$app->register(new ValidatorServiceProvider());

$app['validator.mapping.class_metadata_factory'] = $app->share(function () {
    return new LazyLoadingMetadataFactory(
        new YamlFileLoader(__DIR__ . '/src/Resources/config/validation.yml')
    );
});

$app->register(new NormalizerServiceProvider());
$app->register(new RepositoryServiceProvider());
$app->register(new ServiceControllerServiceProvider());
$app->register(new ControllerServiceProvider());

if (!empty($_ENV['DEVELOPMENT'])) {
    $app['debug'] = true;
}

if (empty($_ENV['DEVELOPMENT'])) {
    $app->run();
}

return $app;