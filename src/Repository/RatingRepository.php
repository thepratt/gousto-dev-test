<?php

namespace Pratt\Repository;

use Pratt\Entity\Rating;

class RatingRepository extends AbstractRepository
{
    protected $entityClass = Rating::class;

    public function getTotalRatingByRecipeId($id)
    {
        $ratings = $this->data->filter(function (Rating $item) use ($id) {
            return $item->getRecipeId() === $id;
        })->getValues();

        $total = 0;

        foreach ($ratings as $rating) {
            $total += $rating->getRating();
        }

        return ($total /= count($ratings));
    }

    public function exists($id)
    {
        return $this->data->exists(function ($index, $item) use ($id) {
            return $item->getId() === $id;
        });
    }
}