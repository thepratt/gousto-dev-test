<?php

namespace Pratt\Repository;

use Pratt\Entity\Recipe;

class RecipeRepository extends AbstractRepository
{
    protected $entityClass = Recipe::class;

    public function getAllByCuisine($cuisine)
    {
        return $this->data->filter(function (Recipe $item) use ($cuisine) {
            return $item->getRecipeCuisine() === $cuisine;
        })->getValues();
    }
}