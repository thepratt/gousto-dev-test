<?php

namespace Pratt\Repository;

use Doctrine\Common\Collections\ArrayCollection;
use Pratt\Entity\EntityInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

abstract class AbstractRepository
{
    protected $entityClass;
    protected $normalizer;
    protected $file;
    protected $data;
    protected $headers = [];

    public function __construct(ObjectNormalizer $normalizer, $file)
    {
        $this->normalizer = $normalizer;
        $this->file = $file;
        $this->data = new ArrayCollection();

        $this->parse();
    }

    protected function parse()
    {
        if (empty($this->entityClass)) {
            throw new \InvalidArgumentException;
        }

        foreach (array_map('str_getcsv', file($this->file)) as $index => $row) {
            if (!$index) {
                $this->headers = $row;
            }

            if ($index > 0) {
                $recipe = $this->normalizer->denormalize(array_combine($this->headers, $row), $this->entityClass);
                $this->data->set($recipe->getId(), $recipe);
            }
        }
    }

    public function getById($id)
    {
        return $this->data->get($id);
    }

    public function save(EntityInterface $entity)
    {
        if (!$entity->getId()) {
            $entity->setId($this->data->last()->getId() + 1);
        }

        $this->data->set($entity->getId(), $entity);

        $this->persist();
    }

    public function persist()
    {
        $f = fopen($this->file, 'w');
        $data = array_merge(
            [$this->headers],
            array_map(
                function ($item) {
                    return $this->normalizer->normalize($item);
                },
                $this->data->getValues()
            )
        );

        foreach ($data as $item) {
            fputcsv($f, $item);
        }

        fclose($f);
    }
}