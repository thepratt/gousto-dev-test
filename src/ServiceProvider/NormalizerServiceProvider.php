<?php

namespace Pratt\ServiceProvider;

use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class NormalizerServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['normalizer'] = $app->share(function () {
            return new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
        });
    }

    public function boot(Application $app)
    {
    }
}