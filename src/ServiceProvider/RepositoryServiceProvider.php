<?php

namespace Pratt\ServiceProvider;

use Pratt\Repository\RatingRepository;
use Pratt\Repository\RecipeRepository;
use Silex\Application;
use Silex\ServiceProviderInterface;

class RepositoryServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['repository.recipe'] = $app->share(function () use ($app) {
            return new RecipeRepository($app['normalizer'], __DIR__ . '/../Resources/recipes.csv');
        });

        $app['repository.rating'] = $app->share(function() use ($app) {
            return new RatingRepository($app['normalizer'], __DIR__ . '/../Resources/ratings.csv');
        });
    }

    public function boot(Application $app)
    {
    }
}