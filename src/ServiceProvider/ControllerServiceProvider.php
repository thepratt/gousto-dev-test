<?php

namespace Pratt\ServiceProvider;

use Pratt\Controller\AppController;
use Pratt\Controller\RecipeController;
use Pratt\Controller\RecipeRatingController;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\RouteCollection;

class ControllerServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $app['controller.app'] = $app->share(function () use ($app) {
            return new AppController();
        });

        $app['controller.recipe'] = $app->share(function () use ($app) {
            return new RecipeController($app['repository.recipe'], $app['normalizer'], $app['validator']);
        });

        $app['controller.recipe.rating'] = $app->share(function () use ($app) {
            return new RecipeRatingController($app['repository.rating'], $app['normalizer'], $app['validator']);
        });

        $app['routes'] = $app->extend('routes', function (RouteCollection $routes, Application $app) {
            $loader = new YamlFileLoader(new FileLocator(__DIR__ . '/../Resources/config'));
            $routes->addCollection($loader->load('routing.yml'));

            return $routes;
        });
    }

    public function boot(Application $app)
    {
    }
}