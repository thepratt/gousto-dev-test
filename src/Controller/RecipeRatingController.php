<?php

namespace Pratt\Controller;

use Pratt\Entity\Rating;
use Pratt\Helper\ErrorFormatHelper;
use Pratt\Repository\RatingRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class RecipeRatingController
{
    private $repository;
    private $normalizer;
    private $validator;

    public function __construct(
        RatingRepository $repository,
        ObjectNormalizer $normalizer,
        $validator
    ) {
        $this->repository = $repository;
        $this->normalizer = $normalizer;
        $this->validator = $validator;
    }

    public function getAction(Request $request, $id)
    {
        if (!$this->repository->exists($id)) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse([
            'id' => $id,
            'rating' => $this->repository->getTotalRatingByRecipeId($id)
        ], Response::HTTP_OK);
    }

    public function addAction(Request $request, $id)
    {
        if (!$this->repository->exists($id)) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        $content = $request->getContent();

        $json = json_decode($content, true);
        $rating = $this->normalizer->denormalize($json, Rating::class);
        $rating->setRecipeId($id);

        $errors = $this->validator->validate($rating);
        if ($errors->count() > 0) {
            return new JsonResponse([
                'errors' => ErrorFormatHelper::format($errors)
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->repository->save($rating);

        return new JsonResponse([
            'id' => $id,
            'rating' => $this->repository->getTotalRatingByRecipeId($id)
        ], Response::HTTP_OK);
    }
}