<?php

namespace Pratt\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AppController
{
    public function indexAction(Application $app)
    {
        return new JsonResponse([
            "name" => "Sam Pratt",
            "date" => "27-01-2016"
        ], Response::HTTP_OK);
    }
}