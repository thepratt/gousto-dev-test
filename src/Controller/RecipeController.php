<?php

namespace Pratt\Controller;

use Pratt\Entity\Recipe;
use Pratt\Helper\ErrorFormatHelper;
use Pratt\Helper\PaginationHelper;
use Pratt\Repository\RecipeRepository;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class RecipeController
{
    private $repository;
    private $normalizer;
    private $validator;

    public function __construct(
        RecipeRepository $repository,
        ObjectNormalizer $normalizer,
        $validator
    ) {
        $this->repository = $repository;
        $this->normalizer = $normalizer;
        $this->validator = $validator;
    }

    public function getAction(Request $request, $id)
    {
        $recipe = $this->repository->getById($id);

        if (is_null($recipe)) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse($this->normalizer->normalize($recipe), Response::HTTP_OK);
    }

    public function getAllByCuisineAction(Request $request)
    {
        $cuisine = $request->query->get('cuisine') ?: 'asian';
        $page = $request->query->get('page') ?: 1;

        $recipes = $this->repository->getAllByCuisine($cuisine);

        if (empty($recipes)) {
            return new JsonResponse(null, Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse(
            array_map(function ($recipe) {
                return $this->normalizer->normalize($recipe);
            }, PaginationHelper::paginate($recipes, $page)),
            Response::HTTP_OK
        );
    }

    public function createAction(Request $request)
    {
        $content = $request->getContent();

        $json = json_decode($content, true);
        $recipe = $this->normalizer->denormalize($json, Recipe::class);

        $errors = $this->validator->validate($recipe);
        if ($errors->count() > 0) {
            return new JsonResponse([
                'errors' => ErrorFormatHelper::format($errors)
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->repository->save($recipe);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

    public function updateAction(Request $request, $id)
    {
        $content = $request->getContent();

        $json = json_decode($content, true);
        $recipe = $this->normalizer->denormalize($json, Recipe::class);
        $recipe->setId($id);

        $errors = $this->validator->validate($recipe);
        if ($errors->count() > 0) {
            return new JsonResponse([
                'errors' => ErrorFormatHelper::format($errors)
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->repository->save($recipe);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}