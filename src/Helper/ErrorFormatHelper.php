<?php

namespace Pratt\Helper;

use Symfony\Component\Validator\ConstraintViolationList;

class ErrorFormatHelper
{
    public static function format(ConstraintViolationList $errorList)
    {
        $errors = [];

        foreach ($errorList as $error) {
            $errors[$error->getPropertyPath()] = $error->getMessage();
        }

        return $errors;
    }
}