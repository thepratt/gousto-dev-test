<?php

namespace Pratt\Helper;

class PaginationHelper
{
    const PAGE_SIZE = 1;

    public static function paginate(Array $array, $page)
    {
        return array_slice($array, ($page - 1) * self::PAGE_SIZE, self::PAGE_SIZE);
    }
}