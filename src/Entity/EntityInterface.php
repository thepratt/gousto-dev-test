<?php

namespace Pratt\Entity;

interface EntityInterface
{
    public function getId();

    public function setId($id);
}